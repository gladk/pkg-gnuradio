From 242eeaaa06c7faa1425158b0d2d12c30a39c6a1c Mon Sep 17 00:00:00 2001
From: Michael Dickens <michael.dickens@ettus.com>
Date: Wed, 14 Aug 2019 20:46:11 -0400
Subject: [PATCH 17/19] RPC: fixes to allow using Thrift 0.9 through 0.12

---
 cmake/Modules/FindTHRIFT.cmake                |   6 +
 .../include/gnuradio/CMakeLists.txt           |   1 +
 .../gnuradio/rpc_shared_ptr_selection.h       | 148 ++++++++++++++++++
 .../include/gnuradio/rpcmanager_base.h        |   4 +-
 .../include/gnuradio/rpcserver_base.h         |   3 +-
 .../gnuradio/rpcserver_booter_aggregator.h    |   4 +-
 .../include/gnuradio/thrift_server_template.h |  22 +--
 7 files changed, 173 insertions(+), 15 deletions(-)
 create mode 100644 gnuradio-runtime/include/gnuradio/rpc_shared_ptr_selection.h

diff --git a/cmake/Modules/FindTHRIFT.cmake b/cmake/Modules/FindTHRIFT.cmake
index 970a9c4c3..f9b4f85d8 100644
--- a/cmake/Modules/FindTHRIFT.cmake
+++ b/cmake/Modules/FindTHRIFT.cmake
@@ -91,3 +91,9 @@ if (THRIFT_FOUND AND NOT TARGET Thrift::thrift)
     INTERFACE_LINK_LIBRARIES "${THRIFT_LIBRARIES}"
     )
 endif()
+
+# set version to be useable by calling script
+
+IF(THRIFT_FOUND)
+  set(THRIFT_VERSION ${PC_THRIFT_VERSION} CACHE INTERNAL "Thrift Version" FORCE)
+ENDIF()
diff --git a/gnuradio-runtime/include/gnuradio/CMakeLists.txt b/gnuradio-runtime/include/gnuradio/CMakeLists.txt
index 045f140e7..9770bf7cf 100644
--- a/gnuradio-runtime/include/gnuradio/CMakeLists.txt
+++ b/gnuradio-runtime/include/gnuradio/CMakeLists.txt
@@ -72,6 +72,7 @@ install(FILES
   tag_checker.h
   types.h
   unittests.h
+  rpc_shared_ptr_selection.h
   rpccallbackregister_base.h
   rpcmanager_base.h
   rpcmanager.h
diff --git a/gnuradio-runtime/include/gnuradio/rpc_shared_ptr_selection.h b/gnuradio-runtime/include/gnuradio/rpc_shared_ptr_selection.h
new file mode 100644
index 000000000..5ce264cc1
--- /dev/null
+++ b/gnuradio-runtime/include/gnuradio/rpc_shared_ptr_selection.h
@@ -0,0 +1,148 @@
+/* -*- c++ -*- */
+/*
+ * Copyright 2019 Free Software Foundation, Inc.
+ *
+ * This file is part of GNU Radio
+ *
+ * GNU Radio is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 3, or (at your option)
+ * any later version.
+ *
+ * GNU Radio is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License
+ * along with GNU Radio; see the file COPYING.  If not, write to
+ * the Free Software Foundation, Inc., 51 Franklin Street,
+ * Boston, MA 02110-1301, USA.
+ */
+
+#ifndef RPC_SHARED_PTR_SELECTION_H
+#define RPC_SHARED_PTR_SELECTION_H
+
+#include <memory>
+
+#include "gnuradio/config.h"
+#ifdef GR_RPCSERVER_THRIFT
+#include <thrift/version.h>
+// to get boost includes, if and only if they're still in Thrift:
+#include <thrift/concurrency/Thread.h>
+#endif
+
+// select a "shared_ptr" type to use: std:: or boost::
+//
+// Dear Thrift maintainers, should you read this, here's a haiku:
+//
+// Version numbers carved in strings
+// C Preprocessor can't
+// Templates can't
+// Sadness
+
+
+namespace gr {
+
+//! \brief constexpr check for whether a character is a digit
+constexpr bool digit(char d) { return !(d < '0' || d > '9'); }
+
+/* \brief Convert C string to a Version magic, constexpr.
+ *
+ * Converts "5.10.3" to 5·10⁶ + 10·10³+ 3 = 5 010 003
+ */
+constexpr uint64_t cstr_to_version_magic(const char* cstr, uint64_t magicsofar)
+{
+    // OK, we go: check for the current character being a zero byte, in that case,
+    // return magicsofar
+    return *cstr ?
+                 // next, check whether a digit, in that case, multiply magicsofar with
+                 // 10, and parse on into the sunset
+               (digit(*cstr)
+                    ? cstr_to_version_magic(cstr + 1, magicsofar * 10 + (*cstr - '0'))
+                    :
+                    // if it's not a digit, maybe it's a dot? throw
+                    ((*cstr) == '.' ?
+                                    // it's a dot -> multiply by one hundred, move on
+                         cstr_to_version_magic(cstr + 1, magicsofar * 100)
+                                    :
+                                    // not a dot, not a digit, not an end byte
+                         (throw "String is not a x.y.z version string")))
+                 : magicsofar;
+}
+
+/* \brief Convert three-element version numbers to a single unsigned integer. constexpr.
+ *
+ * Converts 5, 10, 3 to 5·10⁶ + 10·10³+ 3 = 5 010 003
+ */
+constexpr uint64_t
+version_to_version_magic(unsigned int x, unsigned int y, unsigned int z)
+{
+    return (uint64_t)x * 1000 * 1000 + y * 1000 + z;
+}
+
+/* \brief Thrift-specific version check
+ *
+ * Evaluates THRIFT_VERSION as string by converting it to a version magic.
+ *
+ * Then, compares that to 0.11.0, which is the first release where Thrift might use
+ * std::shared_ptrs.
+ *
+ * Then, compares to 0.13.0, which is the release where Thrift only uses
+ * std::shared_ptr.
+ *
+ * If inbetween, do the same check that Thrift does in their 0.11-0.12
+ * stdcxx.h headers, minus the redundant MSVC check, which is guaranteed by our MSVC
+ * minimum requirement, anyway
+ */
+constexpr bool thrift_version_uses_boost()
+{
+#ifndef THRIFT_VERSION
+    // if there's no thrift, then that doesn't use boost
+    return false;
+#else // THRIFT_VERSION
+    return (cstr_to_version_magic(THRIFT_VERSION, 0) <
+            version_to_version_magic(0, 11, 0)) ||
+           (cstr_to_version_magic(THRIFT_VERSION, 0) <
+                version_to_version_magic(0, 13, 0) &&
+    /* This is the internal check that thrift does in 0.11 and 0.12 to decide whether to
+      use std:: or boost::shared_ptr. As soon as we require Thrift >= 0.13.0 this
+      #define-based check is to be removed. */
+#if defined(BOOST_NO_CXX11_SMART_PTR) || defined(FORCE_BOOST_SMART_PTR)
+            true
+#else
+            false
+#endif
+           );
+#endif // THRIFT_VERSION
+}
+
+/* \brief template base class; unspecialized; specializations hold a `t` member that's
+ * actually a Thrift-compatible `shared_ptr`.
+ */
+template <bool use_std, class T>
+struct rpc_sptr_impl;
+
+//! \brief template specialization holding a `std::shared_ptr`
+template <class T>
+struct rpc_sptr_impl<true, T> {
+    using t = std::shared_ptr<T>;
+};
+
+#ifdef BOOST_SHARED_PTR_HPP_INCLUDED
+//! \brief template specialization holding a `boost::shared_ptr`
+template <class T>
+struct rpc_sptr_impl<false, T> {
+    using t = boost::shared_ptr<T>;
+};
+#endif
+
+/* \brief template class with ::t member type that is std:: or boost::shared_ptr,
+ * depending on `thrift_version_uses_boost()`.
+ */
+template <class T>
+struct rpc_sptr {
+    using t = typename rpc_sptr_impl<!thrift_version_uses_boost(), T>::t;
+};
+} // namespace gr
+#endif
diff --git a/gnuradio-runtime/include/gnuradio/rpcmanager_base.h b/gnuradio-runtime/include/gnuradio/rpcmanager_base.h
index e92df5bd1..8c60fe821 100644
--- a/gnuradio-runtime/include/gnuradio/rpcmanager_base.h
+++ b/gnuradio-runtime/include/gnuradio/rpcmanager_base.h
@@ -23,7 +23,7 @@
 #ifndef RPCMANAGER_BASE_H
 #define RPCMANAGER_BASE_H
 
-#include <boost/shared_ptr.hpp>
+#include <gnuradio/rpc_shared_ptr_selection.h>
 
 class rpcserver_booter_base;
 // class rpcserver_booter_aggregator;
@@ -31,7 +31,7 @@ class rpcserver_booter_base;
 class rpcmanager_base
 {
 public:
-    typedef boost::shared_ptr<rpcserver_booter_base> rpcserver_booter_base_sptr;
+    typedef gr::rpc_sptr<rpcserver_booter_base>::t rpcserver_booter_base_sptr;
 
     rpcmanager_base() { ; }
     ~rpcmanager_base() { ; }
diff --git a/gnuradio-runtime/include/gnuradio/rpcserver_base.h b/gnuradio-runtime/include/gnuradio/rpcserver_base.h
index 46b088474..f11fd0b6f 100644
--- a/gnuradio-runtime/include/gnuradio/rpcserver_base.h
+++ b/gnuradio-runtime/include/gnuradio/rpcserver_base.h
@@ -23,6 +23,7 @@
 #ifndef RPCSERVER_BASE_H
 #define RPCSERVER_BASE_H
 
+#include <gnuradio/rpc_shared_ptr_selection.h>
 #include <gnuradio/rpccallbackregister_base.h>
 
 class rpcserver_base : public virtual callbackregister_base
@@ -45,7 +46,7 @@ public:
 
     virtual void setCurPrivLevel(const priv_lvl_t priv) { cur_priv = priv; }
 
-    typedef boost::shared_ptr<rpcserver_base> rpcserver_base_sptr;
+    typedef gr::rpc_sptr<rpcserver_base>::t rpcserver_base_sptr;
 
 protected:
     priv_lvl_t cur_priv;
diff --git a/gnuradio-runtime/include/gnuradio/rpcserver_booter_aggregator.h b/gnuradio-runtime/include/gnuradio/rpcserver_booter_aggregator.h
index 0fbbb604e..fa20278be 100644
--- a/gnuradio-runtime/include/gnuradio/rpcserver_booter_aggregator.h
+++ b/gnuradio-runtime/include/gnuradio/rpcserver_booter_aggregator.h
@@ -24,9 +24,9 @@
 #define RPCSERVER_BOOTER_AGGREGATOR
 
 #include <gnuradio/api.h>
+#include <gnuradio/rpc_shared_ptr_selection.h>
 #include <gnuradio/rpcserver_aggregator.h>
 #include <gnuradio/rpcserver_booter_base.h>
-#include <boost/shared_ptr.hpp>
 #include <string>
 
 class rpcserver_server;
@@ -49,7 +49,7 @@ protected:
 
 private:
     std::string d_type;
-    boost::shared_ptr<rpcserver_aggregator> server;
+    gr::rpc_sptr<rpcserver_aggregator>::t server;
 };
 
 #endif /* RPCSERVER_BOOTER_AGGREGATOR */
diff --git a/gnuradio-runtime/include/gnuradio/thrift_server_template.h b/gnuradio-runtime/include/gnuradio/thrift_server_template.h
index 1c87e4820..fc3b7e8a7 100644
--- a/gnuradio-runtime/include/gnuradio/thrift_server_template.h
+++ b/gnuradio-runtime/include/gnuradio/thrift_server_template.h
@@ -25,6 +25,7 @@
 
 #include <gnuradio/logger.h>
 #include <gnuradio/prefs.h>
+#include <gnuradio/rpc_shared_ptr_selection.h>
 #include <gnuradio/thrift_application_base.h>
 #include <iostream>
 
@@ -50,11 +51,12 @@ protected:
     friend class thrift_application_base<TserverBase, TImplClass>;
 
 private:
-    boost::shared_ptr<TserverClass> d_handler;
-    boost::shared_ptr<thrift::TProcessor> d_processor;
-    boost::shared_ptr<thrift::transport::TServerTransport> d_serverTransport;
-    boost::shared_ptr<thrift::transport::TTransportFactory> d_transportFactory;
-    boost::shared_ptr<thrift::protocol::TProtocolFactory> d_protocolFactory;
+    // typename necessary in C++11 for dependent types:
+    typename gr::rpc_sptr<TserverClass>::t d_handler;
+    gr::rpc_sptr<thrift::TProcessor>::t d_processor;
+    gr::rpc_sptr<thrift::transport::TServerTransport>::t d_serverTransport;
+    gr::rpc_sptr<thrift::transport::TTransportFactory>::t d_transportFactory;
+    gr::rpc_sptr<thrift::protocol::TProtocolFactory>::t d_protocolFactory;
     /**
      * Custom TransportFactory that allows you to override the default Thrift buffer size
      * of 512 bytes.
@@ -71,10 +73,10 @@ private:
 
         virtual ~TBufferedTransportFactory() {}
 
-        virtual boost::shared_ptr<thrift::transport::TTransport>
-        getTransport(boost::shared_ptr<thrift::transport::TTransport> trans)
+        virtual gr::rpc_sptr<thrift::transport::TTransport>::t
+        getTransport(gr::rpc_sptr<thrift::transport::TTransport>::t trans)
         {
-            return boost::shared_ptr<thrift::transport::TTransport>(
+            return gr::rpc_sptr<thrift::transport::TTransport>::t(
                 new thrift::transport::TBufferedTransport(trans, bufferSize));
         }
 
@@ -133,11 +135,11 @@ thrift_server_template<TserverBase, TserverClass, TImplClass>::thrift_server_tem
                 d_processor, d_serverTransport, d_transportFactory, d_protocolFactory));
     } else {
         // std::cout << "Thrift Multi-threaded server : " << d_nthreads << std::endl;
-        boost::shared_ptr<thrift::concurrency::ThreadManager> threadManager(
+        gr::rpc_sptr<thrift::concurrency::ThreadManager>::t threadManager(
             thrift::concurrency::ThreadManager::newSimpleThreadManager(nthreads));
 
         threadManager->threadFactory(
-            boost::shared_ptr<thrift::concurrency::PlatformThreadFactory>(
+            gr::rpc_sptr<thrift::concurrency::PlatformThreadFactory>::t(
                 new thrift::concurrency::PlatformThreadFactory()));
 
         threadManager->start();
-- 
2.20.1

