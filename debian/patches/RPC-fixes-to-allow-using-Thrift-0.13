From c321893690de2e591988789758b5f3dd4dc3de0a Mon Sep 17 00:00:00 2001
From: "A. Maitland Bottoms" <bottoms@debian.org>
Date: Sun, 15 Nov 2020 10:58:46 -0500
Subject: [PATCH] RPC: fixes to allow using Thrift 0.13

---
 cmake/Modules/FindTHRIFT.cmake                     | 14 +++++++++++++-
 config.h.in                                        |  6 ++++++
 .../include/gnuradio/rpc_shared_ptr_selection.h    |  2 ++
 .../include/gnuradio/thrift_server_template.h      | 10 ++++++++++
 4 files changed, 31 insertions(+), 1 deletion(-)

diff --git a/cmake/Modules/FindTHRIFT.cmake b/cmake/Modules/FindTHRIFT.cmake
index f9b4f85d8..ef39090be 100644
--- a/cmake/Modules/FindTHRIFT.cmake
+++ b/cmake/Modules/FindTHRIFT.cmake
@@ -70,7 +70,19 @@ ENDIF  (CMAKE_CROSSCOMPILING)
 
 # Set to found if we've made it this far
 if(THRIFT_INCLUDE_DIRS AND THRIFT_LIBRARIES AND PYTHON_THRIFT_FOUND)
-  set(THRIFT_FOUND TRUE CACHE BOOL "If Thift has been found")
+  set(THRIFT_FOUND TRUE CACHE BOOL "If Thrift has been found")
+
+  find_file(THRIFT_HAS_VERSION_H thrift/version.h
+    PATH ${THRIFT_INCLUDE_DIRS} NO_DEFAULT_PATH)
+  if(THRIFT_HAS_VERSION_H-FOUND)
+    set(THRIFT_HAS_VERSION_H TRUE CACHE BOOL "If Thrift has thrift/version.h")
+  endif()
+
+  find_file(THRIFT_HAS_THREADFACTORY_H thrift/concurrency/ThreadFactory.h
+    PATH ${THRIFT_INCLUDE_DIRS} NO_DEFAULT_PATH)
+  if(THRIFT_HAS_THREADFACTORY_H-FOUND)
+    set(THRIFT_HAS_THREADFACTORY_H TRUE CACHE BOOL "If Thrift has thrift/concurrency/ThreadFactory.h")
+  endif()
 endif(THRIFT_INCLUDE_DIRS AND THRIFT_LIBRARIES AND PYTHON_THRIFT_FOUND)
 
 
diff --git a/config.h.in b/config.h.in
index e3570941a..996374639 100644
--- a/config.h.in
+++ b/config.h.in
@@ -36,6 +36,12 @@
 #ifndef GR_RPCSERVER_THRIFT
 #cmakedefine GR_RPCSERVER_THRIFT
 #endif
+#ifndef THRIFT_HAS_VERSION_H
+#cmakedefine THRIFT_HAS_VERSION_H
+#endif
+#ifndef THRIFT_HAS_THREADFACTORY_H
+#cmakedefine THRIFT_HAS_THREADFACTORY_H
+#endif
 #ifndef GR_MPLIB_GMP
 #cmakedefine GR_MPLIB_GMP
 #endif
diff --git a/gnuradio-runtime/include/gnuradio/rpc_shared_ptr_selection.h b/gnuradio-runtime/include/gnuradio/rpc_shared_ptr_selection.h
index 5ce264cc1..9d14e6696 100644
--- a/gnuradio-runtime/include/gnuradio/rpc_shared_ptr_selection.h
+++ b/gnuradio-runtime/include/gnuradio/rpc_shared_ptr_selection.h
@@ -27,7 +27,9 @@
 
 #include "gnuradio/config.h"
 #ifdef GR_RPCSERVER_THRIFT
+#ifdef THRIFT_HAS_VERSION_H
 #include <thrift/version.h>
+#endif
 // to get boost includes, if and only if they're still in Thrift:
 #include <thrift/concurrency/Thread.h>
 #endif
diff --git a/gnuradio-runtime/include/gnuradio/thrift_server_template.h b/gnuradio-runtime/include/gnuradio/thrift_server_template.h
index fc3b7e8a7..319d519af 100644
--- a/gnuradio-runtime/include/gnuradio/thrift_server_template.h
+++ b/gnuradio-runtime/include/gnuradio/thrift_server_template.h
@@ -23,6 +23,7 @@
 #ifndef THRIFT_SERVER_TEMPLATE_H
 #define THRIFT_SERVER_TEMPLATE_H
 
+#include <gnuradio/config.h>
 #include <gnuradio/logger.h>
 #include <gnuradio/prefs.h>
 #include <gnuradio/rpc_shared_ptr_selection.h>
@@ -30,7 +31,11 @@
 #include <iostream>
 
 #include "thrift/ControlPort.h"
+#ifdef THRIFT_HAS_THREADFACTORY_H
+#include <thrift/concurrency/ThreadFactory.h>
+#else
 #include <thrift/concurrency/PlatformThreadFactory.h>
+#endif
 #include <thrift/concurrency/ThreadManager.h>
 #include <thrift/server/TSimpleServer.h>
 #include <thrift/server/TThreadPoolServer.h>
@@ -138,9 +143,14 @@ thrift_server_template<TserverBase, TserverClass, TImplClass>::thrift_server_tem
         gr::rpc_sptr<thrift::concurrency::ThreadManager>::t threadManager(
             thrift::concurrency::ThreadManager::newSimpleThreadManager(nthreads));
 
+#ifdef THRIFT_HAS_THREADFACTORY_H
+        threadManager->threadFactory(gr::rpc_sptr<thrift::concurrency::ThreadFactory>::t(
+            new thrift::concurrency::ThreadFactory()));
+#else
         threadManager->threadFactory(
             gr::rpc_sptr<thrift::concurrency::PlatformThreadFactory>::t(
                 new thrift::concurrency::PlatformThreadFactory()));
+#endif
 
         threadManager->start();
 
-- 
2.20.1

