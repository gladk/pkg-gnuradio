From 4b42f1b8dca395ea2c6e409c2f5c3e7393a394db Mon Sep 17 00:00:00 2001
From: Jacob Gilbert <mrjacobagilbert@gmail.com>
Date: Mon, 12 Oct 2020 15:42:57 -0700
Subject: [PATCH 13/19] runtime: fix latency issue caused by setting block
 alias on message blocks

This changes how the blocks are identified when they need to be notified that they have messages waiting. It also clarifies the name the block is registered with explicitly, and adds exceptions for looking up blocks that do not exist.
---
 gnuradio-runtime/lib/basic_block.cc    |  2 +-
 gnuradio-runtime/lib/block.cc          |  4 ++--
 gnuradio-runtime/lib/block_registry.cc | 13 ++++++++++++-
 3 files changed, 15 insertions(+), 4 deletions(-)

diff --git a/gnuradio-runtime/lib/basic_block.cc b/gnuradio-runtime/lib/basic_block.cc
index bc1661765..69c985a49 100644
--- a/gnuradio-runtime/lib/basic_block.cc
+++ b/gnuradio-runtime/lib/basic_block.cc
@@ -197,7 +197,7 @@ void basic_block::insert_tail(pmt::pmt_t which_port, pmt::pmt_t msg)
     msg_queue_ready[which_port]->notify_one();
 
     // wake up thread if BLKD_IN or BLKD_OUT
-    global_block_registry.notify_blk(alias());
+    global_block_registry.notify_blk(d_symbol_name);
 }
 
 pmt::pmt_t basic_block::delete_head_nowait(pmt::pmt_t which_port)
diff --git a/gnuradio-runtime/lib/block.cc b/gnuradio-runtime/lib/block.cc
index 591428390..7020b5f6a 100644
--- a/gnuradio-runtime/lib/block.cc
+++ b/gnuradio-runtime/lib/block.cc
@@ -59,7 +59,7 @@ block::block(const std::string& name,
       d_pmt_done(pmt::intern("done")),
       d_system_port(pmt::intern("system"))
 {
-    global_block_registry.register_primitive(alias(), this);
+    global_block_registry.register_primitive(d_symbol_name, this);
     message_port_register_in(d_system_port);
     set_msg_handler(d_system_port, boost::bind(&block::system_handler, this, _1));
 
@@ -612,7 +612,7 @@ void block::system_handler(pmt::pmt_t msg)
     pmt::pmt_t op = pmt::car(msg);
     if (pmt::eqv(op, d_pmt_done)) {
         d_finished = pmt::to_long(pmt::cdr(msg));
-        global_block_registry.notify_blk(alias());
+        global_block_registry.notify_blk(d_symbol_name);
     } else {
         std::cout << "WARNING: bad message op on system port!\n";
         pmt::print(msg);
diff --git a/gnuradio-runtime/lib/block_registry.cc b/gnuradio-runtime/lib/block_registry.cc
index f25ad25b8..d1216fdb2 100644
--- a/gnuradio-runtime/lib/block_registry.cc
+++ b/gnuradio-runtime/lib/block_registry.cc
@@ -132,10 +132,21 @@ void block_registry::notify_blk(std::string blk)
     gr::thread::scoped_lock guard(d_mutex);
 
     if (primitive_map.find(blk) == primitive_map.end()) {
+        /* In later GNU Radio versions, we throw here:
+         *
+         * throw std::runtime_error("block notify failed: block not found!");
+         *
+         * But since this is an API change that we'd like to avoid on a maintenance
+         * branch, we're not doing that on maint-3.8.
+         */
         return;
     }
-    if (primitive_map[blk]->detail().get())
+    if (primitive_map[blk]->detail().get()) {
         primitive_map[blk]->detail()->d_tpb.notify_msg();
+    } else {
+        // not having block detail is not necessarily a problem; this will happen when
+        // publishing a message to a block that exists but has not yet been started
+    }
 }
 
 } /* namespace gr */
-- 
2.20.1

